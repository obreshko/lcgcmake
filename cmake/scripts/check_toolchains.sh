#!/bin/sh

checkUrlFromFile(){
    # args:
    #   filename (from *-urlinfo.txt list)
    local filename="$1"
    local url="$(source "$filename" && echo $module)"
    echo -n "Checking URL '$url' ... "
    if curl --output /dev/null --silent --head --fail "$url"; then
        echo "OK"
    else
        echo "Fail"
        exit 1
    fi
}

gitbranch="$(git rev-parse --abbrev-ref HEAD)"

toolchain_list=""
if echo $gitbranch | grep -q "^LCG_"; then
  toolchain_list="$(echo $gitbranch | cut -d_ -f 2)"
else
  base_toolchains="experimental|dev3|dev4|dev3python3"
  toolchain_list="$(ls cmake/toolchain/heptools-* | grep -Ev "common|contrib" | grep -E "(${base_toolchains})\." | xargs -n 1 basename | sed 's/.*-\(.*\)\..*/\1/g')"
fi

BASEDIR="$PWD"
sourcedir="$BASEDIR"
for toolchain in ${toolchain_list}; do
  echo " Checking toolchain ${toolchain} ... "
  builddir="$BASEDIR/build_${toolchain}"
  installdir="$BASEDIR/install_${toolchain}"
  mkdir -p "$builddir"
  cd "$builddir"
  cmake -DLCG_VERSION=${toolchain} \
        -DCMAKE_INSTALL_PREFIX=$installdir \
        $sourcedir
  if [ $? -ne 0 ]; then
      echo "Configure failed. Exit."
      exit 1
  fi
  echo "Checking all URLs ..."
  find $builddir -name '*-urlinfo.txt' | while read filename; do
      checkUrlFromFile "$filename"
  done
  echo "Checking empty hashes ..."
  python $sourcedir/cmake/scripts/check_hashes.py `ls $builddir/LCG_${toolchain}*.txt | grep -v contrib`
  if [ $? -ne 0 ]; then
    exit 1
  fi
done

