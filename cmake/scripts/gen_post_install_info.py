#!/usr/bin/env python
'''
Python rewrite of the original post-install.sh.
'''
import os
import sys

LISTNAME = '.filelist'

def skip_dir(root, name):
    '''
    Tell if a directory should not be searched for files.
    
    @param root: parent path
    @param name: name of the directory
    '''
    # FIXME: can the logic be simplified?
    return ((name == 'logs') or
            ('datafiles' in name) or
            (root.endswith('share') and (
                name.startswith('LHAPDF') or
                name.startswith('sources'))
            ))


def generate(lcg_home, pkg_home, path_map):
    '''
    Do something...
    
    @param lcg_home: root of LCG installation
    '''
    log_file = open(os.path.join(pkg_home, "gen-post-install.log"), "w")
    list_file = open(os.path.join(pkg_home, LISTNAME), 'w')
    list_file.write(lcg_home + '\n')

    # remove empty key
    path_map.pop('', None)
    # add real (no-symlink) names to the mappings
    path_map.update((os.path.realpath(old), new)
                    for old, new in path_map.items())
    
    #for old, new in path_map.items():
    #    print >> log_file, "#"+old, "->", new
    #    if (not old) or (not new):
    #        print >> log_file, "#Invalid map entry:", old, "->", new
    #        print "Invalid map entry:", old, "->", new
    
    # write all the mappings
    list_file.writelines('%s->%s\n' % (old, new)
                         for old, new in path_map.items() if old and new and not old.startswith('/usr') and not new.startswith('/usr'))
    
    for root, dirs, files in os.walk(pkg_home):
        dirs[:] = [d for d in dirs
                   if not skip_dir(root, d)]
        for filename in files:
            # FIXME we could quickly ignore files from the name
            # e.g. estension == '.so'
            
            filename = os.path.join(root, filename)
            
            content = open(filename, 'rb').read()
            if '\0' in content:
                # it's a binary (or UTF-16) file
                # See http://stackoverflow.com/q/898669
                continue
            
            for old in path_map:
                if old in content:
                    list_file.write(os.path.relpath(filename, pkg_home) + '\n')
                    break


if __name__ == '__main__':
    # FIXME: do we want many arguments or one space separated list?
    path_map = {}
    for arg in sys.argv[3:]:
        path_map.update(spec.split(':', 1)
                        for spec in arg.split())
        
    generate(sys.argv[1], sys.argv[2], path_map)
