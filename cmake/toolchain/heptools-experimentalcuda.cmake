cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the structure)
set(heptools_version   experimentalcuda)
set(LCG_PYTHON_VERSION 2)

# Common setup for all 
include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# External packages (experimental versions)
include(${CMAKE_CURRENT_LIST_DIR}/heptools-experimental-base.cmake)
LCG_external_package(cuda              9.2                                  )
LCG_external_package(cudnn             7.4.1.5                              )

# Additional external packages (Generators)
include(${CMAKE_CURRENT_LIST_DIR}/heptools-experimental-generators.cmake)

# Prepare the search paths according to the versions above
LCG_prepare_paths()
